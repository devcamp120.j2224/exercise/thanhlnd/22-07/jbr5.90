package com.example.demo.Service;
import java.util.ArrayList;
import java.util.List;
import com.example.demo.Model.Album;
public class AlbumService {

    public static ArrayList<Album> getAlbumList(){

        // tạo 1 liststring add vào String tự do , để khi khởi tạo Albulm thì add vào 
        List<String> list1 = new ArrayList<String>();
        List<String> list2 = new ArrayList<String>();
        List<String> list3 = new ArrayList<String>();
        List<String> list4 = new ArrayList<String>();
        List<String> list5 = new ArrayList<String>();
        List<String> list6 = new ArrayList<String>();
        List<String> list7 = new ArrayList<String>();
        List<String> list8 = new ArrayList<String>();
        List<String> list9 = new ArrayList<String>();
        List<String> list10 = new ArrayList<String>();

        list1.add("track 1 , list 1 ");
        list2.add("track 2 , list 2");
        list3.add("track 3 , list 3");
        list4.add("track 4 , list 4");
        list5.add("track 5 , list 5");
        list6.add("track 6 , list 6");
        list7.add("track 7 , list 7");
        list8.add("track 8 , list 8");
        list9.add("track 9 , list 9");
        list10.add("track 10 , list 10");
        
        Album album1 = new Album("Hết thương cạn nhớ", list1);
        Album album2 = new Album("Hương", list2);
        Album album3 = new Album("Yêu lại từ đầu", list3);
        Album album4 = new Album("Cơn mưa tình yêu", list4);
        Album album5 = new Album("Ngôi nhà hoa hồng", list5);
        Album album6 = new Album("Công chúa bong bóng", list6);
        Album album7 = new Album("Nỗi nhớ đầy vơi", list7);
        Album album8 = new Album("Thu cuối", list8);
        Album album9 = new Album("Cơn mưa ngang qua", list9);
        Album album10 = new Album("Chỉ là không cùng nhau", list10);
        
        ArrayList<Album> albums = new ArrayList<>();

        albums.add(album1);
        albums.add(album2);
        albums.add(album3);
        albums.add(album4);
        albums.add(album5);
        albums.add(album6);
        albums.add(album7);
        albums.add(album8);
        albums.add(album9);
        albums.add(album10);

        return albums ;
    }

    public static ArrayList<Album> getAlbumNooPhuocThinh(){

        // tạo list String 
        List<String> list1 = new ArrayList<String>();
        List<String> list2 = new ArrayList<String>();
        List<String> list3 = new ArrayList<String>();

        // add dữ liệu vào list
        list1.add("Chờ ngày mưa tan");
        list2.add("Thương em là điều anh không ngờ");
        list3.add("Như phút ban đầu");

        // khởi tạo đối tượng album
        Album vol1 = new Album("vol 1", list1);
        Album vol2 = new Album("vol 2", list2);
        Album vol3 = new Album("vol 3", list3);

        // khỏi tạo arraylist
        ArrayList<Album> albums = new ArrayList<>();

        // add đối tượng albyum vùa tạo vào arraylist 
        albums.add(vol1);
        albums.add(vol2);
        albums.add(vol3);

        return albums ;
    }
    // giống getAlbum phía trên
    public static ArrayList<Album> getAlbumBaoAnh(){

        List<String> List4 = new ArrayList<String>();
        List<String> List5 = new ArrayList<String>();
        List<String> List6 = new ArrayList<String>();

        List4.add("Như lời đồn");
        List5.add("Yêu một người vô tâm");
        List6.add("Anh muốn em sống sao");

        Album vol4 = new Album("vol 4", List4);
        Album vol5 = new Album("vol 5", List5);
        Album vol6 = new Album("vol 6", List6);

        ArrayList<Album> albums = new ArrayList<>();

        albums.add(vol4);
        albums.add(vol5);
        albums.add(vol6);

        return albums ;
    }
    // giống getAlbum phía trên
    public static ArrayList<Album> getAlbumMyTam(){

        List<String> List7 = new ArrayList<String>();
        List<String> List8 = new ArrayList<String>();
        List<String> List9 = new ArrayList<String>();
        List<String> List10 = new ArrayList<String>();

        List7.add("Như chưa bắt đầu");
        List8.add("Ước gì");
        List9.add("Hẹn ước từ hư vô");
        List10.add("Đừng hỏi em");

        Album vol7 = new Album("vol 9", List7);
        Album vol8 = new Album("vol 8", List8);
        Album vol9 = new Album("vol 9", List9);
        Album vol10 = new Album("vol 10", List10);

        ArrayList<Album> albums = new ArrayList<>();

        albums.add(vol7);
        albums.add(vol8);
        albums.add(vol9);
        albums.add(vol10);

        return albums ;
    }
  

}
